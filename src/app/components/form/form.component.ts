import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/app/interface/persona.interface';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  persona: Persona = {
    nombre: '',
    correo: ''
  }

  save():void {
    console.log('ENVIO DEL SUBMIT');
    console.log(this.persona.nombre);
    console.log(this.persona.correo);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
